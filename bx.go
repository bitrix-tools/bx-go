package bx

import (
	"bx-api/pkg/methods"
)

func NewClient(APIURL string) methods.CRM {
	return &methods.Bitrix24{
		ApiUrl: APIURL,
	}
}
