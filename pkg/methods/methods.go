package methods

import (
	"encoding/json"

	"github.com/valyala/fasthttp"
)

type CRM interface {
	Do(methodJob string, requestData interface{}, responseData interface{}) error
}

type Bitrix24 struct {
	ApiUrl string
}

var contentTypeJSON = []byte("application/json")

func (m *Bitrix24) Do(methodJob string, requestData interface{}, responseData interface{}) error {
	req := fasthttp.AcquireRequest()
	url := m.ApiUrl + "/" + methodJob
	req.SetRequestURI(url)
	req.Header.SetMethod(fasthttp.MethodPost)
	req.Header.SetUserAgentBytes([]byte("bx-api"))
	req.Header.SetContentTypeBytes(contentTypeJSON)
	req.Header.Set("Accept", "application/json")
	jsonRequest, err := json.Marshal(requestData)
	if err != nil {
		return err
	}
	req.SetBody(jsonRequest)
	resp := fasthttp.AcquireResponse()
	defer func() {
		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}()
	err = fasthttp.Do(req, resp)
	if err != nil {
		return err
	}

	return json.Unmarshal(resp.Body(), responseData)
}
