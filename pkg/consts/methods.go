package consts

const (
	Auth = "auth"

	UserAdmin = "user.admin.json"

	PlacementBind   = "placement.bind"
	PlacementUnBind = "placement.unbind"

	CrmContactGet    = "crm.contact.get"
	CrmContactList   = "crm.contact.list"
	CrmContactAdd    = "crm.contact.add"
	CrmContactUpdate = "crm.contact.update"

	CrmLeadGet    = "crm.lead.get"
	CrmLeadAdd    = "crm.lead.add"
	CrmLeadList   = "crm.lead.list"
	CrmLeadUpdate = "crm.lead.update"

	CrmDealAdd    = "crm.deal.add"
	CrmDealGet    = "crm.deal.get"
	CrmDealList   = "crm.deal.list"
	CrmDealUpdate = "crm.deal.update"

	CrmCompanyGet    = "crm.company.get"
	CrmCompanyAdd    = "crm.company.add"
	CrmCompanyList   = "crm.company.list"
	CrmCompanyUpdate = "crm.company.update"

	EventBind   = "event.bind.json"
	EventUnBind = "event.unbind.json"

	UserFieldTypeAdd       = "userfieldtype.add"
	UserFieldTypeDelete    = "userfieldtype.delete"
	CrmContactUserFieldAdd = "crm.contact.userfield.add"
	CrmLeadUserFieldAdd    = "crm.lead.userfield.add"
	CrmDealUserFieldAdd    = "crm.deal.userfield.add"
	CrmCompanyUserFieldAdd = "crm.company.userfield.add"

	CrmContactUserFieldList = "crm.contact.userfield.list"
	CrmLeadUserFieldList    = "crm.lead.userfield.list"
	CrmDealUserFieldList    = "crm.deal.userfield.list"
	CrmCompanyUserFieldList = "crm.company.userfield.list"

	CrmContactUserFieldDelete = "crm.contact.userfield.delete"
	CrmLeadUserFieldDelete    = "crm.lead.userfield.delete"
	CrmDealUserFieldDelete    = "crm.deal.userfield.delete"
	CrmCompanyUserFieldDelete = "crm.company.userfield.delete"

	UserFieldConfigList   = "userfieldconfig.list"
	UserFieldConfigDelete = "userfieldconfig.delete"

	BizprocRobotAdd  = "bizproc.robot.add"
	BizprocRobotDel  = "bizproc.robot.delete"
	BizProcEventSend = "bizproc.event.send"

	CrmDuplicatesFindByComm = "crm.duplicate.findbycomm"

	CrmItemGet = "crm.item.get"
)
